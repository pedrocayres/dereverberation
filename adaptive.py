import numpy as np
import scipy.signal as sig

# Adapt filter with the normalized least mean squares (NLMS) method
def nlms_adapt(x, d, m, beta, epsilon=0, save_history=False,
        step_history=100):
    n = x.shape[0]
    out = np.zeros(n - m + 1)
    if save_history:
        samples_history = (n - m) // step_history + 1
        w = np.zeros((samples_history, m))
        e = np.zeros(samples_history)
        w_k = np.zeros(m)
        for k in range(n - m + 1):
            x_k = x[k:k + m]
            out[k] = w_k.dot(x_k)
            e_k = d[k + m - 1] - out[k]
            if k % step_history == 0:
                 index_sample = k // step_history
                 w[index_sample, :] = w_k
                 e[index_sample] = e_k
            w_k += beta * x_k * e_k / (x_k.dot(x_k) + epsilon)
        w = w[:, ::-1]
    else:
        w_k = np.zeros(m)
        for k in range(n - m + 1):
            x_k = x[k:k + m]
            out[k] = w_k.dot(x_k)
            e_k = d[k + m - 1] - out[k]
            w_k += beta * x_k * e_k / (x_k.dot(x_k) + epsilon)
        w = w_k[::-1]
        e = e_k
    return w, e, out

# Adapt filter with NLMS modified by adding a L1 term for the block energy in
# the objective function (NLMS+BEO_L1)
def nlms_beo_l1_adapt(x, d, m, n_b, beta, alpha, gamma, epsilon=0,
        delta=0, save_history=False, step_history=100):
    n = x.shape[0]
    out = np.zeros(n - m + 1)
    q_b = gamma.shape[0]
    if save_history:
        samples_history = (n - m) // step_history + 1
        w = np.zeros((samples_history, m))
        e = np.zeros(samples_history)
        w_k = np.zeros(m)
        gamma = gamma[::-1]
        for k in range(n - m + 1):
            x_k = x[k:k + m]
            out[k] = w_k.dot(x_k)
            e_k = d[k + m - 1] - out[k]
            if k % step_history == 0:
                 index_sample = k // step_history
                 w[index_sample, :] = w_k
                 e[index_sample] = e_k
            aux = (w_k.reshape(q_b, -1) ** 2).sum(axis=1)
            s = 2 * (aux >= gamma) - 1
            s[aux <= delta * gamma] = 0
            alpha_s = np.repeat(alpha*s, n_b)
            lambda_half = ((beta * e_k + (alpha_s / (1 + alpha_s)
                * w_k).dot(x_k)) / ((x_k / (1 + alpha_s)).dot(x_k) + epsilon))
            w_k = (w_k + x_k * lambda_half) / (1 + alpha_s)
        w = w[:, ::-1]
    else:
        w_k = np.zeros(m)
        gamma = gamma[::-1]
        for k in range(n - m + 1):
            x_k = x[k:k + m]
            out[k] = w_k.dot(x_k)
            e_k = d[k + m - 1] - out[k]
            aux = (w_k.reshape(q_b, -1) ** 2).sum(axis=1)
            s = 2 * (aux >= gamma) - 1
            s[aux <= delta * gamma] = 0
            alpha_s = np.repeat(alpha*s, n_b)
            lambda_half = ((beta * e_k + (alpha_s / (1 + alpha_s)
                * w_k).dot(x_k)) / ((x_k / (1 + alpha_s)).dot(x_k) + epsilon))
            w_k = (w_k + x_k * lambda_half) / (1 + alpha_s)
        w = w_k[::-1]
        e = e_k
    return w, e, out

# Adapt filter with the affine projection algorithm (APA)
def apa_adapt(x, d, m, beta, k_eq, epsilon=0, save_history=False,
        step_history=100):
    epsilon = np.eye(k_eq) * epsilon
    n = x.shape[0]
    out = np.zeros(n - m + 1)
    if save_history:
        samples_history = (n - m) // step_history + 1
        w = np.zeros((samples_history, m))
        e = np.zeros(samples_history)
        w_k = np.zeros(m)
        x_k = np.zeros((k_eq, m))
        for k in range(n - m + 1):
            x_k[:-1, :] = x_k[1:, :]
            x_k[-1, :] = x[k:k + m]
            out_k = x_k.dot(w_k)
            e_k = d[k + m - k_eq:k + m] - out_k
            out[k] = out_k[-1]
            if k % step_history == 0:
                index_sample = k // step_history
                w[index_sample, :] = w_k
                e[index_sample] = e_k[-1]
            w_k += beta * x_k.T.dot(np.linalg.lstsq(x_k.dot(x_k.T) + epsilon,
                e_k, rcond=None)[0])
        w = w[:, ::-1]
    else:
        w_k = np.zeros(m)
        x_k = np.zeros((k_eq, m))
        for k in range(n - m + 1):
            x_k[:-1, :] = x_k[1:, :]
            x_k[-1, :] = x[k:k + m]
            out_k = x_k.dot(w_k)
            e_k = d[k + m - k_eq:k + m] - out_k
            out[k] = out_k[-1]
            w_k += beta * x_k.T.dot(np.linalg.lstsq(x_k.dot(x_k.T) + epsilon,
                e_k, rcond=None)[0])
        w = w_k[::-1]
        e = e_k
    return w, e, out

# Adapt filter with APA+BEO_L1
def apa_beo_l1_adapt(x, d, m, n_b, beta, k_eq, alpha, gamma, epsilon=0,
        delta=0, save_history=False, step_history=100):
    epsilon = np.eye(k_eq) * epsilon
    n = x.shape[0]
    out = np.zeros(n - m + 1)
    q_b = gamma.shape[0]
    if save_history:
        samples_history = (n - m) // step_history + 1
        w = np.zeros((samples_history, m))
        e = np.zeros(samples_history)
        w_k = np.zeros(m)
        x_k = np.zeros((k_eq, m))
        gamma = gamma[::-1]
        for k in range(n - m + 1):
            x_k[:-1, :] = x_k[1:, :]
            x_k[-1, :] = x[k:k + m]
            out_k = x_k.dot(w_k)
            e_k = d[k + m - k_eq:k + m] - out_k
            out[k] = out_k[-1]
            if k % step_history == 0:
                 index_sample = k // step_history
                 w[index_sample, :] = w_k
                 e[index_sample] = e_k[-1]
            aux = (w_k.reshape(q_b, -1) ** 2).sum(axis=1)
            s = 2 * (aux >= gamma) - 1
            s[aux <= delta * gamma] = 0
            alpha_s = np.repeat(alpha*s, n_b)
            lambda_half = np.linalg.lstsq(x_k.dot((x_k / (1 + alpha_s)).T) +
                epsilon, beta * e_k + x_k.dot(alpha_s / (1 + alpha_s) * w_k),
                rcond=None)[0]
            w_k = (w_k + x_k.T.dot(lambda_half)) / (1 + alpha_s)
        w = w[:, ::-1]
    else:
        w_k = np.zeros(m)
        x_k = np.zeros((k_eq, m))
        gamma = gamma[::-1]
        for k in range(n - m + 1):
            x_k[:-1, :] = x_k[1:, :]
            x_k[-1, :] = x[k:k + m]
            out_k = x_k.dot(w_k)
            e_k = d[k + m - k_eq:k + m] - out_k
            out[k] = out_k[-1]
            aux = (w_k.reshape(q_b, -1) ** 2).sum(axis=1)
            s = 2 * (aux >= gamma) - 1
            s[aux <= delta * gamma] = 0
            alpha_s = np.repeat(alpha*s, n_b)
            lambda_half = np.linalg.lstsq(x_k.dot((x_k / (1 + alpha_s)).T) +
                epsilon, beta * e_k + x_k.dot(alpha_s / (1 + alpha_s) * w_k),
                rcond=None)[0]
            w_k = (w_k + x_k.T.dot(lambda_half)) / (1 + alpha_s)
        w = w_k[::-1]
        e = e_k
    return w, e, out
