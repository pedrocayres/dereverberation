import time
import numpy as np
import scipy.signal as sig
import scipy.io.wavfile as wav
import matplotlib.pyplot as plt
import tikzplotlib

def perform_benchmark(n, sigma_v, h, t, step_history, param, method,
        color_filter=None, verbose=False):
    for i in range(t):
        ## Generate signals for optimization
        x = np.random.randn(n)
        if color_filter is not None:
            x = sig.convolve(x, color_filter)[:n]
        d = sig.convolve(x, h)[:n] + sigma_v * np.random.randn(n)
        if verbose:
            print('Signal variance: {:.6f}.'.format(d.var()))

        ## Optimize filter
        adapt_time = time.time()
        w_i, e_i, _ = method(x[:n], d, **param, save_history=True,
                step_history=step_history)
        adapt_time = time.time() - adapt_time
        if verbose:
            print('Time elapsed: {:.2f}s.'.format(adapt_time))

        ## Generate signals for testing
        x = np.random.randn(n)
        if color_filter is not None:
            x = sig.convolve(x[:n], color_filter)[:n]
        d = sig.convolve(x, h)[:n] + sigma_v * np.random.randn(n)
        dp = sig.convolve(x, w_i[-1, :])[:n]
        mse_i = ((d - dp) ** 2).mean()
        if verbose:
            print('MSE of the final filter: {:.6f}.'.format(mse_i))
        w_i = w_i.reshape(1, -1, w_i.shape[1])
        e_i = e_i.reshape(1, -1)
        if i == 0:
            w = w_i
            e = e_i
            mse = mse_i
        else:
            w = np.concatenate((w, w_i), axis=0)
            e = np.concatenate((e, e_i), axis=0)
            mse += mse_i
    mse /= t
    return w, e, mse

def experiment_on_noise(method_list, n, sigma_v, h, t, step_history,
        color_filter=None, plot_asymptotic_nlms=False,
        title={'MSE':'MSE evolution', 'MSD':'MSD evolution',
        'coef':'Evolution of Coefficient'}, coef_plot_list=None, filepath=None,
        verbose=False):
    m = method_list[0]['param']['m']
    beta = method_list[0]['param']['beta']
    result_list = []
    for method in method_list:
        w, e, mse = perform_benchmark(n, sigma_v, h, t, step_history,
            method['param'], method['function'], color_filter, verbose)
        print('{} mean MSE ({} samples): {:.6f}.'.format(method['name'],
            t, mse))
        mse = (e ** 2).mean(axis=0)
        msd = ((w - h[:m]) ** 2).mean(axis=0).sum(axis=1)
        result_list.append({'MSE':mse, 'MSD':msd})
        if coef_plot_list is not None:
            result_list[-1]['coef'] = []
            for i in coef_plot_list:
                result_list[-1]['coef'].append(w.mean(axis=0)[:, i])
    time_sample = np.arange(0, n - m + 1, step_history)
    for mea in ['MSE', 'MSD']:
        plt.title(title[mea])
        plt.yscale('log')
        for i in range(len(method_list)):
            plt.plot(time_sample, result_list[i][mea],
                label=method_list[i]['name'])
        if (plot_asymptotic_nlms and mea == 'MSE'):
            plt.plot(time_sample, np.ones(time_sample.shape) * (2 * sigma_v **
                2) / (2 - beta), label='Theoretical asymptotic MSE for NLMS',
                linestyle=':')
        elif (plot_asymptotic_nlms and mea == 'MSD'):
            plt.plot(time_sample, np.ones(time_sample.shape) * (beta * sigma_v
                ** 2) / (2 - beta),
                label='Theoretical asymptotic MSD for NLMS',
                linestyle=':')
        plt.legend()
        plt.xlabel('Iteration')
        plt.grid()
        if (filepath is not None):
            tikzplotlib.save(filepath[mea])
        plt.show()
    if coef_plot_list is not None:
        for i, j in enumerate(coef_plot_list):
            plt.title('{} {}'.format(title['coef'], j))
            for k in range(len(method_list)):
                plt.plot(time_sample, result_list[k]['coef'][i],
                    label=r'$w_{{{}}}$ ({})'.format(j, method_list[k]['name']))
            plt.plot(time_sample, h[j] * np.ones(time_sample.shape),
                label='$w^*_{{{}}}$'.format(j), linestyle=':')
            plt.legend()
            plt.xlabel('Iteration')
            plt.grid()
            plt.show()
    return result_list

def experiment_on_signal(method_list, x, h, sigma_v, step_history,
        title={'MSE':'MSE evolution', 'MSD':'MSD evolution',
        'coef':'Evolution of Coefficient'}, coef_plot_list=None,
        filepath_plot=None,filepath_wav=None, rate=0.0):
    def to_int_16(a):
        return np.clip(np.around(2 ** 15 * a), -2 ** 15, 2 ** 15 - 1).astype(
            np.int16)
    m = method_list[0]['param']['m']
    n = x.shape[0]
    d = np.convolve(x, h)[:n] + sigma_v * np.random.randn(n)
    print('Original signal variance: {:.6f}'.format(x.var()))
    print('Room signal variance: {:.6f}'.format(d.var()))
    if filepath_wav is not None:
        wav.write(filepath_wav['room'], rate, to_int_16(d))
    result_list = []
    for method in method_list:
        adapt_time = time.time()
        w, e, d_adapt = method['function'](x, d, **method['param'],
            save_history=True, step_history=step_history)
        adapt_time = time.time() - adapt_time
        print('Time elapsed ({}): {:.2f}s'.format(method['name'], adapt_time))
        if filepath_wav is not None:
            wav.write(filepath_wav[method['name']], rate, to_int_16(d_adapt))
        result_list.append({'MSE':e**2, 'MSD':((w-h[:m])**2).sum(axis=1)})
        if coef_plot_list is not None:
            result_list[-1]['coef'] = []
            for i in coef_plot_list:
                result_list[-1]['coef'].append(w[:, i])
    time_sample = np.arange(0, n - m + 1, step_history)
    for mea in ['MSE', 'MSD']:
        plt.title(title[mea])
        plt.yscale('log')
        for i in range(len(method_list)):
            plt.plot(time_sample, result_list[i][mea],
                label=method_list[i]['name'])
        plt.legend()
        plt.xlabel('Iteration')
        plt.grid()
        if filepath_plot is not None:
            tikzplotlib.save(filepath_plot[mea])
        plt.show()
    if coef_plot_list is not None:
        for i, j in enumerate(coef_plot_list):
            plt.title('{} {}'.format(title['coef'], j))
            for k in range(len(method_list)):
                plt.plot(time_sample, result_list[k]['coef'][i],
                    label=r'$w_{{{}}}$ ({})'.format(j, method_list[k]['name']))
            plt.plot(time_sample, h[j] * np.ones(time_sample.shape),
                label='$w^*_{{{}}}$'.format(j), linestyle=':')
            plt.legend()
            plt.xlabel('Iteration')
            plt.grid()
            plt.show()
    return result_list
