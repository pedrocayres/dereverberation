import os
from scipy.io import loadmat
from scipy.signal import resample_poly

# Load a RIR from the AIR database
def load_air(airpar, air_path):
    rir_type_list = ['binaural', 'phone']
    room_list = ['booth', 'office', 'meeting', 'lecture', 'stairway',
            'stairway1', 'stairway2', 'corridor', 'bathroom', 'lecture1',
            'aula_carolina']
    rir_string = rir_type_list[airpar['rir_type'] - 1]
    room_string = room_list[airpar['room'] - 1]

    if rir_string == 'phone':
        pos_list = ['hhp', 'hfrp']
        pos_string = pos_list[airpar['phone_pos'] - 1]
        if 'mock_up_type' not in airpar:
            airpar['mock_up_type'] = 1
        if airpar['mock_up_type'] == 1:
            file_name = 'air_phone'
        elif airpar['mock_up_type'] == 2:
            file_name = 'air_phone_BT'
        file_name = '{}_{}_{}_{}'.format(file_name, room_string, pos_string,
                airpar['channel'])
    elif rir_string == 'binaural':
        file_name = 'air_binaural_{}_{}_{}_{}'.format(room_string,
                airpar['channel'], airpar['head'], airpar['rir_no'])
        if room_string == 'stairway1' or room_string == 'aula_carolina':
            file_name = '{}_{}'.format(file_name, airpar['azimuth'])
            if room_string == 'aula_carolina':
                file_name = '{}_{}'.format(file_name, airpar['mic_type'])

    # Load RIR
    file_name = '{}.mat'.format(file_name)
    file_path = os.path.join(air_path, file_name)
    data_dict = loadmat(file_path, squeeze_me=True)
    h_air = data_dict['h_air']
    air_info = data_dict['air_info']

    # Resample to match the given sampling frequency
    if air_info['fs'] != airpar['fs']:
        h_air = resample_poly(h_air, airpar['fs'], air_info['fs'], axis=0)
        air_info['fs'] = airpar['fs']

    return h_air, air_info
